# How to clone de project and start the container docker

## Clone the project

```console
git clone git@gitlab.com:robsonburatti/next-level-week/next-level-week-mobile.git
```

## Install the packages of dependencies of the project

```console
npm i
```

## Create image docker locale

```console
docker build -t registry.gitlab.com/robsonburatti/next-level-week/next-level-week-mobile .
```

## Run container docker

```console
docker run -it -p -p 9003:3000 9003:19000 -p 9003:19001 -p 9003:19002 -v $(pwd):/app registry.gitlab.com/robsonburatti/next-level-week/next-level-week-mobile
```

---

# Create project with structure node

```console
npm init -y
```

# Install the packages of dependencies of the your project

```console
npm i
```

# Create the file `Dockerfile` with the configurations

```yaml
FROM node:9-slim
WORKDIR /app
COPY package.json /app
RUN npm install
COPY . /app
CMD [ "npm", "start" ]
```

# Create de image docker

```console
docker build -t <project-name-here> .
```

# Run your container docker

```console
docker run -it -p 9000:3000 -v $(pwd):/app <project-name-here>
```

# Project configuration scripts

## Install `expo` by running the command

```console
npm install -g expo-cli
```

## Test to identify if the `expo` was installed successfully

```console
expo -h
```

### [Link](https://bit.ly/3bLWTFn) with issues of the most common problems

## Create the project using the `expo`

```console
expo init <project-name-here>
```

### Then the type of template you want to create your project is requested, it can be `blank`

## Run the project with the command

```console
npm start
```

## Install `expo` on the smartphone through of the [Google Play](https://bit.ly/2QXGndq)
